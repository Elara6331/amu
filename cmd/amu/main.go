/*
   AMU: Custom simple markup language
   Copyright (C) 2021 Arsen Musayelyan

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"log"
	"os"

	"go.arsenm.dev/amu"
)

func main() {
	var src *os.File
	// Get source file based on arguments
	if len(os.Args) < 2 {
		src = os.Stdin
	} else {
		// Open file in first argument
		file, err := os.Open(os.Args[1])
		if err != nil {
			log.Fatalln(err)
		}
		// Set source to file
		src = file
	}

	// Convert source to HTML
	html, err := amu.ToHTML(src)
	if err != nil {
		log.Fatalln(err)
	}

	// Print output HTML
	fmt.Println(html)
}