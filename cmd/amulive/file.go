/*
   AMU: Custom simple markup language
   Copyright (C) 2021 Arsen Musayelyan

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"io/ioutil"
	"os"

	"github.com/gotk3/gotk3/gtk"
	sourceview "github.com/linuxerwang/sourceview3"
)

// saveFile opens a file selection dialog if the file has not already
// been saved and saves the current buffer contents at the chosen filename
func saveFile(win gtk.IWindow, buf *sourceview.SourceBuffer) error {
	// If no opened file
	if openedFile == "" {
		// Create new file chooser dialog with two buttons
		fcd, err := gtk.FileChooserDialogNewWith2Buttons(
			"Save",
			win,
			gtk.FILE_CHOOSER_ACTION_SAVE,
			"Cancel",
			gtk.RESPONSE_CANCEL,
			"Save",
			gtk.RESPONSE_APPLY,
		)
		if err != nil {
			return err
		}
		// Do not destroy dialog with parent
		fcd.SetDestroyWithParent(false)
		// Create new file filter
		amuFilter, err := gtk.FileFilterNew()
		if err != nil {
			return err
		}
		// Add pattern to filter
		amuFilter.AddPattern("*.amu")
		// Add filter to dialog
		fcd.AddFilter(amuFilter)
		// Run dialog
		fcdRt := fcd.Run()
		if fcdRt == gtk.RESPONSE_APPLY {
			// Set openedFile to chosen filename
			openedFile = fcd.GetFilename()
		} else {
			// Destroy dialog
			fcd.Destroy()
			// Return no error
			return nil
		}
		// Destroy dialog
		fcd.Destroy()
	}

	// Create chosen file
	file, err := os.Create(openedFile)
	if err != nil {
		return err
	}
	// Get text from source buffer
	amu, err := buf.GetText(buf.GetStartIter(), buf.GetEndIter(), true)
	if err != nil {
		return err
	}
	// Wtite text to file
	_, err = file.Write([]byte(amu))
	if err != nil {
		return err
	}
	// Set buffer modified to false
	buf.SetModified(false)
	return nil
}

// openFile loads the contents of the chosen file into the buffer
func openFile(win gtk.IWindow, buf *sourceview.SourceBuffer) error {
	// Create new file chooser dialog with two buttons
	fcd, err := gtk.FileChooserDialogNewWith2Buttons(
		"Open",
		win,
		gtk.FILE_CHOOSER_ACTION_SAVE,
		"Cancel",
		gtk.RESPONSE_CANCEL,
		"Open",
		gtk.RESPONSE_APPLY,
	)
	if err != nil {
		return err
	}
	// Do not destroy dialog with parent
	fcd.SetDestroyWithParent(false)
	// Create new file filter
	amuFilter, err := gtk.FileFilterNew()
	if err != nil {
		return err
	}
	// Add pattern to file filter
	amuFilter.AddPattern("*.amu")
	// Add file filter to dialog
	fcd.AddFilter(amuFilter)
	// Run dialog
	respType := fcd.Run()
	if respType == gtk.RESPONSE_APPLY {
		// Set openedFile to chosen filename
		openedFile = fcd.GetFilename()
	} else {
		// Destroy dialog
		fcd.Destroy()
		// Return no error
		return nil
	}
	// Destroy dialog
	fcd.Destroy()
	// Read opened file
	data, err := ioutil.ReadFile(openedFile)
	if err != nil {
		return err
	}
	// Set text in buffer
	buf.SetText(string(data))
	// Set buffer modified to false
	buf.SetModified(false)
	return nil
}
