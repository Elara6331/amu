/*
   AMU: Custom simple markup language
   Copyright (C) 2021 Arsen Musayelyan

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import "github.com/gotk3/gotk3/gtk"

type AccelGroup struct {
	*gtk.AccelGroup
}

func NewAccel() (*AccelGroup, error) {
	gtkAg, err := gtk.AccelGroupNew()
	return &AccelGroup{AccelGroup: gtkAg}, err
}

func (ag *AccelGroup) Add(acc string, f interface{}) {
	key, mods := gtk.AcceleratorParse(acc)
	ag.Connect(key, mods, gtk.ACCEL_VISIBLE, f)
}