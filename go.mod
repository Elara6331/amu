module go.arsenm.dev/amu

go 1.17

replace github.com/sqs/gojs => github.com/jbuchbinder/gojs v0.0.0-20201127163838-89502fae67f4

require (
	github.com/alecthomas/chroma v0.9.2
	github.com/gotk3/gotk3 v0.6.1
	github.com/linuxerwang/sourceview3 v0.1.1
	github.com/pkg/browser v0.0.0-20210911075715-681adbf594b8
	github.com/rs/zerolog v1.25.0
	github.com/sourcegraph/go-webkit2 v0.0.0-20170811231113-ade305cf91f4
)

require (
	github.com/alecthomas/repr v0.0.0-20210801044451-80ca428c5142 // indirect
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964 // indirect
	github.com/dlclark/regexp2 v1.4.0 // indirect
	github.com/sqs/gojs v0.0.0-20170522041006-12d0b3282819 // indirect
	golang.org/x/sys v0.0.0-20210616045830-e2b7044e8c71 // indirect
)
