# AMU

AMU is a human readable markup language I made for my own uses and to learn. It is the first lexer and parser that I have written by hand.

---

### Purpose

The purpose of this project is to create a markup language for my own use. I have previously added features to other markup languages, but this caused issues in multiple cases. I decided to create a markup language myself instead.

---

### Usage

This repo is a library for parsing AMU. It can be imported and used like any other go library. 

#### `amu`

In `cmd/amu`, there is a binary that will read from Stdin with no arguments, or from a file given as the first argument. It will output the generated HTML.

#### `amulive`

In `cmd/amulive`, there is a Gtk app that has a text box on the left and a webview on the right. It displays the generated HTML on the right as you type into the text box.

---

### Syntax

The syntax for most of AMU's features can be seen in `test.amu` in this repo.

---

### Performance

Timing this program using `test.amu`, I consistently get 4ms run time. This is much faster than my previous modified markup languages. It is likely unnoticeable in most cases, but interesting nonetheless.