package parser

import (
	"bytes"

	"go.arsenm.dev/amu/scanner"
)

// Attempt to parse an argument list (comma-separated)
func (p *Parser) parseArgs() []string {
	// Create buffer for arguments
	argBuf := &bytes.Buffer{}
	// Create new line slice for arguments
	var args []string

parseLoop:
	for {
		// Scan token
		tok, lit := p.scan()

		// If end of file
		if tok == scanner.EOF {
			// Return nil as this is an invalid argument list
			return nil
		}

		switch tok {
		case scanner.WORD:
			// Write word to argument buffer
			argBuf.WriteString(lit)
		case scanner.WS:
			// Write whitespace to argument buffer
			argBuf.WriteString(lit)
		case scanner.PUNCT:
			// If literal is "]"
			if lit == "]" {
				// If length of argument is greater than 0
				if argBuf.Len() > 0 {
					// Add current argument to slice
					args = append(args, argBuf.String())
				}
				// Stop parsing
				break parseLoop
			} else if lit == "," {
				// Add argument to slice
				args = append(args, argBuf.String())
				// Reset buffer
				argBuf.Reset()
			}
		}
	}

	// Return parsed arguments
	return args
}
