package parser

import (
	"go.arsenm.dev/amu/ast"
	"go.arsenm.dev/amu/scanner"
)

// Attempt to parse heading
func (p *Parser) parseHeading() *ast.Heading {
	// Scan token
	tok, lit := p.scan()

	// Set level to length of HEADING token
	level := len(lit)

	// If token is not HEADING or level is greater than 6
	if tok != scanner.HEADING || level > 6 {
		// Return nil as this is not a valid heading
		return nil
	}

	// Parse para until one newline enocountered
	para := p.parsePara(1)
	// If successful
	if para != nil {
		return &ast.Heading{Level: level, Content: para}
	}

	// Return nil as this is not a valid heading
	return nil
}
