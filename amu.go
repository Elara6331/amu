/*
   AMU: Custom simple markup language
   Copyright (C) 2021 Arsen Musayelyan

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Package amu provides convenience functions for converting amu source to HTML
package amu

import (
	"bytes"
	"io"
	"strings"

	"go.arsenm.dev/amu/ast"
	"go.arsenm.dev/amu/formatter/html"
	"go.arsenm.dev/amu/parser"
)

// Funcs stores functions for use in amu
var Funcs = ast.FuncMap{}

// ToHTML parses the contents of r and formats it as HTML
func ToHTML(r io.Reader) (string, error) {
	// Create new AMU parser
	p := parser.New(r)

	// Parse into AST
	AST, err := p.Parse()
	if err != nil {
		return "", err
	}

	// Create new HTML formatter with AST and Funcs
	formatter := html.NewFormatter(AST, Funcs)

	// Return formatted source
	return formatter.Format(), nil
}

// StringToHTML parses s and formats it as HTML
func StringToHTML(s string) (string, error) {
	return ToHTML(strings.NewReader(s))
}

// BytesToHTML parses the contents of b and formats it as HTML
func BytesToHTML(b []byte) (string, error) {
	return ToHTML(bytes.NewReader(b))
}
