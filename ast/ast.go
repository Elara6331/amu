/*
   AMU: Custom simple markup language
   Copyright (C) 2021 Arsen Musayelyan

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ast

import "fmt"

// FormatType represents the type of
// formatting to be done on a string
type FormatType int

const (
	FormatTypeBold FormatType = iota
	FormatTypeItalic
	FormatTypeMath
	FormatTypeCode
	FormatTypeStrike
)

// AST is the root of the Abstract Syntax Tree for AMU
type AST struct {
	Entries []Entry
}

// Entry represents a single entry in the AST
type Entry struct {
	Heading *Heading
	Image   *Image
	List    *List
	Code    *Code
	Para    *Para
	Break   *Break
	Hline   *Hline
}

// Break represents a line break
type Break struct{}

// Hline represents a horizontal line
type Hline struct{}

// Heading represents a heading
type Heading struct {
	Level   int
	Content *Para
}

// Image represents an image with alternate text
// in case it fails to load and a clickable link
type Image struct {
	Alternate string
	Source    string
	Link      string
}

// List represents a list of items
type List struct {
	Type  string
	Items []ListItem
}

// ListItem represents an item
// with a level in a list
type ListItem struct {
	Level   int
	Content []*Para
}

// Code represents source code to be
// highlighted.
type Code struct {
	Language string
	Style    string
	Text     string
}

// Para represents a paragraph
type Para struct {
	Fragments []ParaFragment
}

// ParaFragment represents parts of
// a paragraph which may or may not
// be text
type ParaFragment struct {
	Word       *string
	Whitespace *string
	Punct      *string
	Link       *Link
	Format     *Format
	Func       *Func
}

// Link represents a hyperlink
type Link struct {
	Link string
	Text string
}

// Format represents formatted text
type Format struct {
	Types []FormatType
	Text  string
}

// Func represents a function call
type Func struct {
	Name string
	Args []string
}

// Funcs represents a place from which to retrieve
// functions for calls
type Funcs interface {
	Set(string, func([]string) string)
	Get(string) (func([]string) string, error)
	Run(string, []string) string
}

// FuncMap is an implementation of Funcs using a map
type FuncMap map[string]func([]string) string

// Set adds a fucntion to the map
func (fm FuncMap) Set(name string, fn func([]string) string) {
	fm[name] = fn
}

// Get gets a function from the map
func (fm FuncMap) Get(name string) (func([]string) string, error) {
	// Get function
	fn, ok := fm[name]
	// If does not exist
	if !ok {
		// Return error
		return nil, fmt.Errorf("func %s does not exist", name)
	}
	// Return function
	return fn, nil
}

// Run runs a function in the map
func (fm FuncMap) Run(name string, args []string) string {
	// Return output of function
	return fm[name](args)
}
